import crypt
import argparse
from threading import Thread

def testPass(cryptPass, dfilename):
    usesSHA = False
    if cryptPass[0] == '$' and cryptPass[2] == '$':
        print "[*] SHA512 password found."
        components = cryptPass.split("$")
        cryptPass = components[3]
        salt = '$' + components[1] + '$' + components[2] +'$'
        usesSHA = True
    else:
        salt = cryptPass[0:2]

    dictFile = open(dfilename, 'r')

    for word in dictFile.readlines():
        word = word.strip('\n')
        cryptWord = crypt.crypt(word,salt)
        
        if usesSHA:
            cryptWord = cryptWord.split(salt)[1]
        
        #print cryptWord

        if (cryptWord == cryptPass):
            print "[+] Found Password: " + word +"\n"

def main():
    parser = argparse.ArgumentParser(description='Perform a ' +
            'dictionary attack against a password file. This ' +
            'encrypts the passwords using the appropriate ' +
            'scheme (auto-detected) and compares to the hash in ' +
            'in the password file. This script is only as useful ' +
            'as your dictionary file')
    parser.add_argument('-p', dest='pfilename', type=str,
            help='specify password file')
    parser.add_argument('-d', dest='dfilename', type=str,
            help='specify dictionary file')
    args = parser.parse_args()

    if (args.pfilename == None) | (args.dfilename == None):
        print parser.usage
        exit(0)
    else:
        pfilename = args.pfilename
        dfilename = args.dfilename

    passFile = open(pfilename)
    for line in passFile.readlines():
        if ":" in line:
            user = line.split(":")[0]
            cryptPass = line.split(":")[1].strip(" ")
            print cryptPass
            print "[*] Cracking password for " + user
            #Need to pass args as tuple, with trailing ',',
            #else it thinks string is tuple, builds accordingly
            t = Thread(target=testPass, args=(cryptPass, dfilename))
            t.start()

if __name__ == "__main__":
    main()
