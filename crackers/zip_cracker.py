import argparse
from threading import Thread
import zipfile

def extractFile(zFile, password):
    try:
        zFile.extractall(pwd=password)
        print '[+] Found password ' + password + '\n'
    except Exception, e:
        pass

def main():
    parser = argparse.ArgumentParser(description='Crack a ' +
            'password on a protected zip file')
    parser.add_argument('-f', dest='zname', type=str,
            help='specify zip file')
    parser.add_argument('-d', dest='dname', type=str,
            help='specify dictionary file')
    args = parser.parse_args()

    if (args.zname == None) | (args.dname == None):
        print parser.usage
        exit(0)
    else:
        zname = args.zname
        dname = args.dname

    zFile = zipfile.ZipFile(zname)
    passFile = open(dname)

    for line in passFile.readlines():
        password = line.strip('\n')
        t = Thread(target=extractFile, args=(zFile, password))
        t.start()

if __name__ == '__main__':
    main()
